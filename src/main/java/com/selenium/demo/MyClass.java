/**
 * 
 */
package com.selenium.demo;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

/**
 * @author home
 *
 */
public class MyClass {
	
	private WebDriver driver = null;


	@Test
	@Parameters({ "myURL", "myTitle" })
	public void testEasy(String myURL, String myTitle) {
		System.out.println("My URl IS: " + myURL);
		driver.get(myURL);
	}

	@BeforeTest
	public void beforeTest() throws Exception {
		System.out.println("Before test");
		System.setProperty("webdriver.chrome.driver", "/home/home/Downloads/chromedriver_linux64/chromedriver");
		driver = new ChromeDriver();
		System.out.println("Exit Before test");
	}

	@AfterTest
	public void afterTest() {
		System.out.println("After test");
		driver.quit();
	}

}
